package com.atguigu.gmall.gateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsConfigurationSource;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;
/**
 * Created by IntelliJ IDEA.
 *
 * @Author 花鸟の虫鱼
 * @CreateDate 21/8/7 13:59
 * @Version 1.0
 * @Description TODO
 */
/**
 * @author atguigu-mqx
 */
@Configuration
public class CorsConfig {
    /*
     * <bean class = "org.springframework.web.cors.reactive.CorsWebFilter"> <bean>
     * */
    @Bean
    public CorsWebFilter corsWebFilter(){

        //  需要创建一个CorsConfiguration
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        //  设置cors 的参数
        corsConfiguration.addAllowedOrigin("*");    //  表示运行访问跨域的域名
        corsConfiguration.addAllowedMethod("*");    //  允许所有的请求方法
        corsConfiguration.addAllowedHeader("*");    //  允许请求头信息
        corsConfiguration.setAllowCredentials(true);    //  允许携带cookie数据

        //  需要创建一个 CorsConfigurationSource 接口的实现类对象
        UrlBasedCorsConfigurationSource urlBasedCorsConfigurationSource = new UrlBasedCorsConfigurationSource();
        //  调用方法
        urlBasedCorsConfigurationSource.registerCorsConfiguration("/**",corsConfiguration);
        //  直接返回对象 new
        return new CorsWebFilter(urlBasedCorsConfigurationSource);
    }

}
