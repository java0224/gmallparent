package com.atguigu.gmall.common.config;


import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author atguigu-mqx
 */
@Component
@Configuration
public class MQProducerAckConfig implements RabbitTemplate.ReturnCallback ,RabbitTemplate.ConfirmCallback {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    //  初始化
    @PostConstruct
    public void init(){
        rabbitTemplate.setReturnCallback(this);
        rabbitTemplate.setConfirmCallback(this);
    }

    /**
     * 确认消息是否发送到交换机！
     * @param correlationData 可以承载消息！
     * @param ack   是否发送到交换机
     * @param cause 原因
     */
    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        //  判断
        if (ack){
            System.out.println("发送成功....");
        }else {
            System.out.println("发送失败.....");
        }

    }

    /**
     * 表示如果消息没有正确的到队列中，才会走这个方法！如果正确被消费了。这个方法是不会走的！
     * @param message
     * @param replyCode
     * @param replyText
     * @param exchange
     * @param routingKey
     */
    @Override
    public void returnedMessage(Message message, int replyCode, String replyText, String exchange, String routingKey) {

        //  判断
        //        if("routing.delay".equals(routingKey)){
        //            //  莫问题！
        //        }else {
        //
        //        }
        System.out.println("消息主体: " + new String(message.getBody()));
        System.out.println("应答码: " + replyCode);
        System.out.println("描述：" + replyText);
        System.out.println("消息使用的交换器 exchange : " + exchange);
        System.out.println("消息使用的路由键 routing : " + routingKey);
    }
}
