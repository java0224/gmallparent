package com.atguigu.gmall.common.service;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author atguigu-mqx
 */
@Service
public class RabbitService {

    //  封装发送消息的方法！
    @Autowired
    private RabbitTemplate rabbitTemplate;

    public boolean sendMessage(String exchange, String routingKey, Object message){
        //  发送消息方法！
        rabbitTemplate.convertAndSend(exchange,routingKey,message);
        //  默认返回
        return true;
    }

    //  发送延迟消息
    public boolean sendDelayMessage(String exchange, String routingKey, Object message, int delayTime) {
        this.rabbitTemplate.convertAndSend(exchange,routingKey,message,(msg) -> {
            //  设置消息的ttl
            msg.getMessageProperties().setDelay(delayTime * 1000);
            return msg;
        });
        //  默认返回
        return true;
    }
}
