package com.atguigu.gmall.common.cache;

import java.lang.annotation.*;

/**
 * @author atguigu-mqx
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface GmallCache {

    //  定义一个属性：原因是为了组成缓存的 key 使用的！
    String prefix() default "cache";
}
