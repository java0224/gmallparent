package com.atguigu.gmall.common.cache;

import com.alibaba.fastjson.JSON;
import com.atguigu.gmall.common.constant.RedisConst;
import lombok.SneakyThrows;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

/**
 * @author atguigu-mqx
 */
@Component
@Aspect
public class GmallCacheAspect {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private RedissonClient redissonClient;

    //  使用环绕通知切这个注解：
    @SneakyThrows
    @Around("@annotation(com.atguigu.gmall.common.cache.GmallCache)")
    public Object cacheAround(ProceedingJoinPoint joinPoint){
        //  定义一个objec 对象
        Object object = null;
        //  在这个方法中，做分布式锁的业务逻辑！
        //  如何获取到是缓存的key！ 这个key 根据注解的前缀有关系！ 先获取到这个注解
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        //  注解用在方法上了。
        GmallCache gmallCache = signature.getMethod().getAnnotation(GmallCache.class);
        //  获取到前缀了
        String prefix = gmallCache.prefix();
        //  缓存的key = prefix + 方法上的参数了！ {参数有可能一样！前缀是我们自己定义的，不能一样}
        String key = prefix + Arrays.asList(joinPoint.getArgs());
        //  获取缓存的数据，进行判断缓存中是否有数据！
        //  获取数据的这个过程封装成一个方法 redisTemplate.opsForValue().get(key);
        try {
            object = this.getRedisData(key,signature);
            //  判断
            if (object==null){
                //  缓存没有数据，查询数据库，放入缓存，防止缓存击穿，缓存穿透！
                String lockKey = prefix + ":lock";
                //  准备上锁
                RLock lock = redissonClient.getLock(lockKey);

                boolean result = lock.tryLock(RedisConst.SKULOCK_EXPIRE_PX1, RedisConst.SKULOCK_EXPIRE_PX2, TimeUnit.SECONDS);
                if (result){
                    try {
                        //  编写业务逻辑 ，要从数据库获取数据
                        //  proceed(): 表示执行我们的方法体！ joinPoint.getArgs() 表示获取方法传递的参数！
                        object = joinPoint.proceed(joinPoint.getArgs());

                        //  判断 防止缓存穿透！
                        if(object==null){
                            Object o = new Object();
                            this.redisTemplate.opsForValue().set(key, JSON.toJSONString(o),RedisConst.SKUKEY_TEMPORARY_TIMEOUT,TimeUnit.SECONDS);
                            return o;
                        }
                        //  如果有数据！
                        this.redisTemplate.opsForValue().set(key, JSON.toJSONString(object),RedisConst.SKUKEY_TIMEOUT,TimeUnit.SECONDS);
                        return object;
                    }finally {
                        //  解锁
                        lock.unlock();
                    }
                }else {
                    //  自旋
                    Thread.sleep(500);
                    return cacheAround(joinPoint);
                }
            }else {
                //  缓存不为空！
                return object;
            }
        } catch (Throwable throwable) {
            //  如果有异常则，记录日志，发送短信通知人员来维护！
            throwable.printStackTrace();
        }
        //  返回
        return joinPoint.proceed(joinPoint.getArgs());
    }

    // 根据key 来获取缓存的数据！
    private Object getRedisData(String key,MethodSignature signature) {
        //  核心获取到缓存数据
        String jsonStr = (String) this.redisTemplate.opsForValue().get(key);

        //  这个jsonStr 到底是个什么类型?
        /*
            @GmallCache;
            SkuInfo getSkuInfo(Long skuId);  SkuInfo
            BaseCategoryView getCategoryViewByCategory3Id(Long category3Id);  BaseCategoryView
            List<SpuSaleAttr> getSpuSaleAttrListCheckBySku(Long skuId, Long spuId); List<SpuSaleAttr>
            Map getSkuValueIdsMap(Long spuId);  Map
         */
        if (!StringUtils.isEmpty(jsonStr)){
            //  获取到方法的返回值类型
            Class returnType = signature.getReturnType();
            //  返回： 将这个字符串 jsonStr 转换为 returnType 这种类型！
            return JSON.parseObject(jsonStr,returnType);
        }
        //  默认返回null！
        return null;
    }
}
