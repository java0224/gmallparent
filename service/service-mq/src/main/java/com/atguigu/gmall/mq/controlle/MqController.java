package com.atguigu.gmall.mq.controlle;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.common.service.RabbitService;
import com.atguigu.gmall.mq.config.DeadLetterMqConfig;
import com.atguigu.gmall.mq.config.DelayedMqConfig;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;
/**
 * Created by IntelliJ IDEA.
 *
 * @Author 花鸟の虫鱼
 * @CreateDate 21/8/25 18:52
 * @Version 1.0
 * @Description TODO
 */
@RestController
@RequestMapping("/mq")
public class MqController {

    @Autowired
    private RabbitService rabbitService;
    @Autowired
    private RabbitTemplate rabbitTemplate;


    //  发送消息的控制器
    @GetMapping("sendConfirm")
    public Result sendConfirm(){
        //  发送消息！
        this.rabbitService.sendMessage("exchange.confirm","routing.confirm6666","来人了，开始接客吧...");
        //  默认返回
        return Result.ok();
    }

    //  发送消息！延迟消息！
    @GetMapping("sendDeadLettle")
    public Result sendDeadLettle(){
        //  记录一个时间！
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //  发送消息
        this.rabbitService.sendMessage(DeadLetterMqConfig.exchange_dead,DeadLetterMqConfig.routing_dead_1,"主人，开门....");
        System.out.println("发送消息的时间:\t"+sdf.format(new Date()));
        return Result.ok();
    }

    //  发送消息    插件方式！
    //  http://localhost:8282/mq/sendDelay
    @GetMapping("sendDelay")
    public Result sendDelay(){
        //  记录一个时间！
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //  发送消息 因为没有设置消息的TTL
        //  this.rabbitService.sendMessage(DelayedMqConfig.exchange_delay,DelayedMqConfig.routing_delay,"主人，开门....");
        this.rabbitTemplate.convertAndSend(DelayedMqConfig.exchange_delay, DelayedMqConfig.routing_delay, "主人，关门吧....", (message)->{
            //  设置消息的ttl
            message.getMessageProperties().setDelay(10000);
            return message;
        });
        System.out.println("发送消息的时间:\t"+sdf.format(new Date()));
        return Result.ok();
    }


}
