package com.atguigu.gmall.mq.receiver;

import com.atguigu.gmall.mq.config.DeadLetterMqConfig;
import com.atguigu.gmall.mq.config.DelayedMqConfig;
import com.rabbitmq.client.Channel;
import lombok.SneakyThrows;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author 花鸟の虫鱼
 * @CreateDate 21/8/25 18:54
 * @Version 1.0
 * @Description TODO
 */
public class ConfirmReceiver {
    //  监听者，消费者！
    @SneakyThrows
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = "queue.confirm",durable = "true",autoDelete = "false"),
            exchange = @Exchange(value = "exchange.confirm"),
            key = {"routing.confirm"}
    ))
    public void getMsg(String msg, Message message, Channel channel){
        System.out.println("接收的消息：\t"+msg);
        System.out.println("接收的消息：\t"+ new String(message.getBody()));

        //  手动确认 第二个参数表示是否批量确认！
        channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
    }

    //  监听消息！ bindings== 在配置文件中已经定义好了！
    @SneakyThrows
    @RabbitListener(queues = DeadLetterMqConfig.queue_dead_2)
    public void getQueue2(String msg,Message message,Channel channel){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println("接收的时间："+sdf.format(new Date())+",接收的消息是：\t"+msg);
        //  手动确认 第二个参数表示是否批量确认！
        channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);

    }
    //  监听消息
    @SneakyThrows
    @RabbitListener(queues = DelayedMqConfig.queue_delay_1)
    public void getDelayMsg(String msg,Message message,Channel channel){
        //  接收到的数据
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println("接收的时间："+sdf.format(new Date())+",接收的消息是：\t"+msg);
        //  手动确认 第二个参数表示是否批量确认！
        channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);

    }
}
