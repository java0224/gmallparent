package com.atguigu.gmall.payment.service;

import com.atguigu.gmall.model.order.OrderInfo;
import com.atguigu.gmall.model.payment.PaymentInfo;

import java.util.Map;

/**
 * @author atguigu-mqx
 */
public interface PaymentService {

    //  保存交易记录！ 本质：向paymentInfo中插入数据！ 与 orderInfo 有关系！
    //  我们现在只有一种类型！
    void savePaymentInfo(OrderInfo orderInfo, String paymentType);

    //  查询交易记录
    PaymentInfo getPaymentInfo(String outTradeNo, String paymentType);

    //  修改状态
    void paySuccess(String outTradeNo, String paymentType, Map<String, String> paramsMap);

    //  修改交易记录
    void updatePaymentInfo(String outTradeNo, String paymentType, PaymentInfo paymentInfo);

    //  关闭电商本地交易记录
    void closePayment(Long orderId);

}
