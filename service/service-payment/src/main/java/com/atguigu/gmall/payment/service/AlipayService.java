package com.atguigu.gmall.payment.service;

import com.alipay.api.AlipayApiException;

/**
 * @author atguigu-mqx
 */
public interface AlipayService {

    //  主要功能生产二维码！
    String createaliPay(Long orderId) throws AlipayApiException;

    //  退款接口
    Boolean refund(Long orderId);

    //  关闭支付宝交易记录
    Boolean closePay(Long orderId);

    //  查询支付宝交易记录
    Boolean checkPayment(Long orderId);
}
