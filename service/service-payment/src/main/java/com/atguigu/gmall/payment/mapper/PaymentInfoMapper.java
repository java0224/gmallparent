package com.atguigu.gmall.payment.mapper;

import com.atguigu.gmall.model.payment.PaymentInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author 花鸟の虫鱼
 * @CreateDate 21/8/27 17:49
 * @Version 1.0
 * @Description TODO
 */
@Mapper
public interface PaymentInfoMapper extends BaseMapper<PaymentInfo> {
}
