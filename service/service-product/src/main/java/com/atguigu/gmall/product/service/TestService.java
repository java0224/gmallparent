package com.atguigu.gmall.product.service;

/**
 * @author atguigu-mqx
 */
public interface TestService {

    /**
     * 测试本地锁
     */
    void testLock();

    /**
     * 读锁
     * @return
     */
    String readLock();

    /**
     * 写锁
     * @return
     */
    String writeLock();
}
