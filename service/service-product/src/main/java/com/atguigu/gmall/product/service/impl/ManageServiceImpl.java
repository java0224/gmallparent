package com.atguigu.gmall.product.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.gmall.common.cache.GmallCache;
import com.atguigu.gmall.common.constant.MqConst;
import com.atguigu.gmall.common.constant.RedisConst;
import com.atguigu.gmall.common.service.RabbitService;
import com.atguigu.gmall.model.product.*;
import com.atguigu.gmall.product.mapper.*;
import com.atguigu.gmall.product.service.ManageService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author atguigu-mqx
 */
@Service
public class ManageServiceImpl implements ManageService {

    //  服务层 调用mapper 层！ 下划线表示预警！
    @Autowired
    private BaseCategory1Mapper baseCategory1Mapper;

    @Autowired
    private BaseCategory2Mapper baseCategory2Mapper;

    @Autowired
    private BaseCategory3Mapper baseCategory3Mapper;

    @Autowired
    private BaseAttrInfoMapper baseAttrInfoMapper;

    @Autowired
    private BaseAttrValueMapper baseAttrValueMapper;

    @Autowired
    private SpuInfoMapper spuInfoMapper;

    @Autowired
    private BaseSaleAttrMapper baseSaleAttrMapper;

    @Autowired
    private SpuImageMapper spuImageMapper;

    @Autowired
    private SpuSaleAttrMapper spuSaleAttrMapper;

    @Autowired
    private SpuSaleAttrValueMapper spuSaleAttrValueMapper;

    @Autowired
    private SkuInfoMapper skuInfoMapper;

    @Autowired
    private SkuImageMapper skuImageMapper;

    @Autowired
    private SkuSaleAttrValueMapper skuSaleAttrValueMapper;

    @Autowired
    private SkuAttrValueMapper skuAttrValueMapper;

    @Autowired
    private BaseCategoryViewMapper baseCategoryViewMapper;

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private RedissonClient redissonClient;

    @Autowired
    private BaseTrademarkMapper baseTrademarkMapper;

    @Autowired
    private RabbitService rabbitService;


    @Override
    public List<BaseCategory1> getBaseCategory1() {
        //  select * from base_category1;
        return baseCategory1Mapper.selectList(null);
    }

    @Override
    public List<BaseCategory2> getBaseCategory2(Long category1Id) {
        //  select * from base_category2 where category1_id = category1Id;
        return baseCategory2Mapper.selectList(new QueryWrapper<BaseCategory2>().eq("category1_id",category1Id));
    }

    @Override
    public List<BaseCategory3> getBaseCategory3(Long category2Id) {
        //  select * from base_category3 where category2_id = category2Id;
        return baseCategory3Mapper.selectList(new QueryWrapper<BaseCategory3>().eq("category2_id",category2Id));
    }

    @Override
    public List<BaseAttrInfo> getBaseAttrInfoList(Long category1Id, Long category2Id, Long category3Id) {
        //  可以使用 mybatis 的动态标签！
        /*
            如果只查询平台属性：
            #   表示根据三级分类Id查询
            select * from base_attr_info where category_id = 61 and category_level = 3;
            #   表示根据二级分类Id查询
            select * from base_attr_info where category_id = 13 and category_level = 2;
            #   表示根据一级分类Id查询
            select * from base_attr_info where category_id = 1 and category_level = 1;

            加点料：
                后续有一个业务查询！ 根据分类Id 查询平台属性，平台属性值
                select * from base_attr_info bai inner join base_attr_value bav on bai.id = bav.attr_id
                where category_id = 61 and category_level = 3;

            平台属性： 平台属性值关系 ？ 1：n
         */
        return baseAttrInfoMapper.selectBaseAttrInfoList(category1Id,category2Id,category3Id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)  //  rollbackFor 表示出现异常会回滚事务！
    public void saveAttrInfo(BaseAttrInfo baseAttrInfo) {
        /*
        做的两张表的插入数据！ mysql 数据库中有一个xxx ,能够保证数据的完整性，一致性！ ACID
        base_attr_info
        base_attr_value
         */
        //  表示已经向base_attr_info 插入了一条数据
        //  判断什么时候走修改，什么时候走新增
        if (baseAttrInfo.getId()==null){
            //  新增
            baseAttrInfoMapper.insert(baseAttrInfo);
        }else {
            //  修改 base_attr_info 一定有Id
            baseAttrInfoMapper.updateById(baseAttrInfo);
        }

        //  int i = 1/0;
        //  获取到平台属性值集合
        List<BaseAttrValue> attrValueList = baseAttrInfo.getAttrValueList();
        //  base_attr_value 先删除对应的平台属性数据，再新增！
        //  delete from baseAttrValue where attr_id = ?
        QueryWrapper<BaseAttrValue> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("attr_id",baseAttrInfo.getId());
        baseAttrValueMapper.delete(queryWrapper);

        //  判断
        if (!CollectionUtils.isEmpty(attrValueList)){
            //  循环遍历
            for (BaseAttrValue baseAttrValue : attrValueList) {
                //  base_attr_value.attr_id  = base_attr_info.id
                baseAttrValue.setAttrId(baseAttrInfo.getId());
                baseAttrValueMapper.insert(baseAttrValue);
            }
        }
    }

    @Override
    public List<BaseAttrValue> getAttrValueList(Long attrId) {

        //  select * from base_attr_value where attr_id = attrId;
        List<BaseAttrValue> baseAttrValueList = baseAttrValueMapper.selectList(new QueryWrapper<BaseAttrValue>().eq("attr_id", attrId));
        //  返回数据
        return baseAttrValueList;
    }

    @Override
    public BaseAttrInfo getBaseAttrInfo(Long attrId) {
        //  获取到平台属性对象
        BaseAttrInfo baseAttrInfo = baseAttrInfoMapper.selectById(attrId);
        //  给平台属性值集合赋值
        baseAttrInfo.setAttrValueList(getAttrValueList(attrId));
        //  返回平台属性对象
        return baseAttrInfo;
    }

    @Override
    public IPage<SpuInfo> getSpuInfoList(Page<SpuInfo> spuInfoPage, SpuInfo spuInfo) {
        //  调用分页查询方法！
        QueryWrapper<SpuInfo> spuInfoQueryWrapper = new QueryWrapper<>();
        spuInfoQueryWrapper.eq("category3_id",spuInfo.getCategory3Id());
        spuInfoQueryWrapper.orderByDesc("id");
        Page<SpuInfo> spuInfoPage1 = spuInfoMapper.selectPage(spuInfoPage, spuInfoQueryWrapper);
        //  返回数据
        return spuInfoPage1;
    }

    @Override
    public List<BaseSaleAttr> getBaseSaleAttrList() {
        //  select * from base_sale_attr;
        return baseSaleAttrMapper.selectList(null);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveSpuInfo(SpuInfo spuInfo) {
        /*
        spu_info
        spu_image
        spu_sale_attr_value
        spu_sale_attr
         */
        spuInfoMapper.insert(spuInfo);

        //  获取spuImage 集合数据
        List<SpuImage> spuImageList = spuInfo.getSpuImageList();
        if (!CollectionUtils.isEmpty(spuImageList)){
            //  循环遍历操作
            for (SpuImage spuImage : spuImageList) {
                //  给spuId 赋值
                spuImage.setSpuId(spuInfo.getId());
                spuImageMapper.insert(spuImage);
            }
        }

        //  获取销售属性列表
        List<SpuSaleAttr> spuSaleAttrList = spuInfo.getSpuSaleAttrList();
        if (!CollectionUtils.isEmpty(spuSaleAttrList)){
            //  循环遍历
            for (SpuSaleAttr spuSaleAttr : spuSaleAttrList) {
                // 给spuId 赋值
                spuSaleAttr.setSpuId(spuInfo.getId());
                spuSaleAttrMapper.insert(spuSaleAttr);

                //  在这获取销售属性对应的销售属性值集合
                List<SpuSaleAttrValue> spuSaleAttrValueList = spuSaleAttr.getSpuSaleAttrValueList();
                if (!CollectionUtils.isEmpty(spuSaleAttrValueList)){
                    for (SpuSaleAttrValue spuSaleAttrValue : spuSaleAttrValueList) {
                        //  给spuId 赋值
                        spuSaleAttrValue.setSpuId(spuInfo.getId());
                        //  给 sale_attr_name 赋值
                        spuSaleAttrValue.setSaleAttrName(spuSaleAttr.getSaleAttrName());
                        spuSaleAttrValueMapper.insert(spuSaleAttrValue);
                    }
                }
            }
        }
    }

    @Override
    public List<SpuImage> getSpuImageList(Long spuId) {
        //  select * from spu_image where spu_id = spuId;
        return spuImageMapper.selectList(new QueryWrapper<SpuImage>().eq("spu_id",spuId));
    }

    @Override
    public List<SpuSaleAttr> getSpuSaleAttrList(Long spuId) {
        //  根据spuId 获取到销售属性集合数据
        return spuSaleAttrMapper.selectSpuSaleAttrList(spuId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveSkuInfo(SkuInfo skuInfo) {
        /*
        skuInfo
        skuImage
        skuSaleAttrValue
        skuAttrValue
         */
        skuInfoMapper.insert(skuInfo);

        //  skuImage
        List<SkuImage> skuImageList = skuInfo.getSkuImageList();
        if (!CollectionUtils.isEmpty(skuImageList)){
            //  循环遍历
            for (SkuImage skuImage : skuImageList) {
                //  设置一个skuId
                skuImage.setSkuId(skuInfo.getId());
                skuImageMapper.insert(skuImage);
            }
        }

        //  skuAttrValue
        List<SkuAttrValue> skuAttrValueList = skuInfo.getSkuAttrValueList();
        if (!CollectionUtils.isEmpty(skuAttrValueList)){
            //  循环遍历
            for (SkuAttrValue skuAttrValue : skuAttrValueList) {
                //  设置一个skuId
                skuAttrValue.setSkuId(skuInfo.getId());
                skuAttrValueMapper.insert(skuAttrValue);

            }
        }

        //  skuSaleAttrValue
        List<SkuSaleAttrValue> skuSaleAttrValueList = skuInfo.getSkuSaleAttrValueList();
        if (!CollectionUtils.isEmpty(skuSaleAttrValueList)){

            for (SkuSaleAttrValue skuSaleAttrValue : skuSaleAttrValueList) {
                //  赋值skuId spuId
                skuSaleAttrValue.setSkuId(skuInfo.getId());
                skuSaleAttrValue.setSpuId(skuInfo.getSpuId());
                skuSaleAttrValueMapper.insert(skuSaleAttrValue);
            }
        }
    }

    @Override
    public IPage getSkuInfoList(Page<SkuInfo> skuInfoPage) {
        //  按照Id 进行降序排序
        QueryWrapper<SkuInfo> skuInfoQueryWrapper = new QueryWrapper<>();
        skuInfoQueryWrapper.orderByDesc("id");
        return skuInfoMapper.selectPage(skuInfoPage,skuInfoQueryWrapper);
    }

    @Override
    public void onSale(Long skuId) {
        //  update sku_info set is_sale = 1 where id = skuId;
        SkuInfo skuInfo = new SkuInfo();
        skuInfo.setId(skuId);
        skuInfo.setIsSale(1);
        skuInfoMapper.updateById(skuInfo);

        //  发送消息！
        //  发送消息主体是谁？ 根据消费者来决定！
        this.rabbitService.sendMessage(MqConst.EXCHANGE_DIRECT_GOODS,MqConst.ROUTING_GOODS_UPPER,skuId);

    }

    @Override
    public void cancelSale(Long skuId) {
        //  update sku_info set is_sale = 0 where id = skuId;
        SkuInfo skuInfo = new SkuInfo();
        skuInfo.setId(skuId);
        skuInfo.setIsSale(0);
        skuInfoMapper.updateById(skuInfo);

        //  发送消息！
        this.rabbitService.sendMessage(MqConst.EXCHANGE_DIRECT_GOODS,MqConst.ROUTING_GOODS_LOWER,skuId);
    }

    @Override
    @GmallCache(prefix = RedisConst.SKUKEY_PREFIX)
    public SkuInfo getSkuInfo(Long skuId) {
        //  获取数据
        //  return getSkuInfoByRedisson(skuId);

        return getSkuInfoDB(skuId);
    }

    private SkuInfo getSkuInfoByRedisson(Long skuId) {
        //        if(true){
        //            //	获取缓存
        //        }else{
        //            //	获取数据库，放入缓存！
        //        }
        SkuInfo skuInfo = null;
        //  先定义缓存的key = sku:skuId:info;  set key value  value = SkuInfo
        String skuKey = RedisConst.SKUKEY_PREFIX+skuId+RedisConst.SKUKEY_SUFFIX;
        try {
            //  第二种方式使用redisson 做分布式锁！
            skuInfo = (SkuInfo) this.redisTemplate.opsForValue().get(skuKey);
            //  判断
            if (skuInfo==null){
                //  获取数据库中的数据，并放入缓存
                //  lockKey = sku:skuId:lock
                String lockKey = RedisConst.SKUKEY_PREFIX+skuId+RedisConst.SKULOCK_SUFFIX;
                RLock lock = redissonClient.getLock(lockKey);
                //  上锁  可重入锁！
                boolean result = lock.tryLock(RedisConst.SKULOCK_EXPIRE_PX1, RedisConst.SKULOCK_EXPIRE_PX2, TimeUnit.SECONDS);
                //  判断
                if(result){
                    try {
                        //   result = true 表示获取到了锁！
                        //   需要查询数据库！将数据放入缓存！   144 数据库没有！ 缓存也没有！
                        skuInfo = this.getSkuInfoDB(skuId);
                        //  判断当前这个skuInfo 是否为空！ 防止缓存穿透！
                        if (skuInfo==null){ //  数据库中根本没有这个数据
                            SkuInfo skuInfo1 = new SkuInfo();
                            //  设置这个key 的过期时间是10分钟
                            //  this.redisTemplate.opsForValue().setIfAbsent(skuKey,skuInfo1,RedisConst.SKUKEY_TEMPORARY_TIMEOUT,TimeUnit.SECONDS);
                            //  缓存中是没有这个key 的！ 我呢，就可以直接使用Set 就可以！
                            //  redisTemplate.opsForValue().set();
                            //  redisTemplate.expire();
                            redisTemplate.opsForValue().set(skuKey,skuInfo1,RedisConst.SKULOCK_EXPIRE_PX1,TimeUnit.SECONDS);
                            //  停止
                            return skuInfo1;
                        }
                        //  数据库中有数据
                        this.redisTemplate.opsForValue().setIfAbsent(skuKey,skuInfo,RedisConst.SKUKEY_TIMEOUT,TimeUnit.SECONDS);
                        //  返回数据
                        return skuInfo;
                    } finally {
                        //  解锁
                        lock.unlock();
                    }
                }else {
                    //  result = false 表示没有获取到了锁！ 设置一个自旋
                    try {
                        Thread.sleep(100);
                        return getSkuInfo(skuId);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }else {
                //  缓存中有数据，直接返回
                return skuInfo;
            }
        } catch (InterruptedException e) {
            System.out.println("redis ---- 宕机了--------记录日志------调用发送短信接口----通知人员来维修----");
            e.printStackTrace();
        }

        //  最后数据库兜底
        return getSkuInfoDB(skuId);
    }

    //  通过redis 做分布式锁案例！
    private SkuInfo getSkuInfoByRedis(Long skuId) {
        //        if(true){
        //            //	获取缓存
        //        }else{
        //            //	获取数据库，放入缓存！
        //        }
        SkuInfo skuInfo = null;
        //  先定义缓存的key = sku:skuId:info;  set key value  value = SkuInfo
        String skuKey = RedisConst.SKUKEY_PREFIX+skuId+RedisConst.SKUKEY_SUFFIX;

        try {
            //  第一种实现方式 使用redis 原生方式！
            skuInfo = (SkuInfo) this.redisTemplate.opsForValue().get(skuKey);
            //  判断
            if (skuInfo==null){
                //  说明缓存中没有数据！  需要查询数据库！{防止缓存击穿：分布式锁} 查询完成之后，将数据放入缓存！
                //  lockKey = sku:skuId:lock
                String lockKey = RedisConst.SKUKEY_PREFIX+skuId+RedisConst.SKULOCK_SUFFIX;
                //  执行上锁的命令：set key value ex/px timeOut nx/xx;
                String uuid = UUID.randomUUID().toString();
                //  调用上锁命令
                Boolean flag = redisTemplate.opsForValue().setIfAbsent(lockKey, uuid, RedisConst.SKULOCK_EXPIRE_PX1, TimeUnit.SECONDS);
                //  判断 flag = true; 表示上锁成功！
                if (flag){
                    //   需要查询数据库！将数据放入缓存！
                    skuInfo = this.getSkuInfoDB(skuId);
                    //  判断当前这个skuInfo 是否为空！ 防止缓存穿透！
                    if (skuInfo==null){ //  数据库中根本没有这个数据
                        SkuInfo skuInfo1 = new SkuInfo();
                        //  设置这个key 的过期时间是10分钟
                        this.redisTemplate.opsForValue().setIfAbsent(skuKey,skuInfo1,RedisConst.SKUKEY_TEMPORARY_TIMEOUT,TimeUnit.SECONDS);
                        //  停止
                        return skuInfo1;
                    }

                    //  如果数据库中有数据！
                    this.redisTemplate.opsForValue().setIfAbsent(skuKey,skuInfo,RedisConst.SKUKEY_TIMEOUT,TimeUnit.SECONDS);

                    //  删除锁！
                    String script = "if redis.call('get',KEYS[1]) == ARGV[1] then return redis.call ('del',KEYS[1]) else return 0 end";
                    //  RedisScript 这个是接口：
                    DefaultRedisScript<Long> redisScript = new DefaultRedisScript<>();
                    //  将lua 脚本放入方法中
                    redisScript.setScriptText(script);
                    //  设置返回值类型
                    redisScript.setResultType(Long.class);

                    //  使用lua 脚本
                    //  第一个参数 RedisScript ，第二个参数 应该是key ,第三个参数：表示key 所对应的值
                    redisTemplate.execute(redisScript, Arrays.asList(skuKey),uuid);
                    //  返回数据
                    return skuInfo;

                }else {
                    //  flag = false;   表示上锁失败！
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    return getSkuInfo(skuId);
                }
            }else{
                //  返回缓存数据！
                return skuInfo;
            }
        } catch (Exception e) {
            System.out.println("redis ---- 宕机了--------记录日志------调用发送短信接口----通知人员来维修----");
            e.printStackTrace();
        }
        //  数据库兜底.
        return getSkuInfoDB(skuId);
    }

    //  根据skuId 获取数据库中的数据！
    private SkuInfo getSkuInfoDB(Long skuId) {
        //  先获取skuInfo
        SkuInfo skuInfo = skuInfoMapper.selectById(skuId);
        //  如何解决空指针问题?
        if (skuInfo!=null){
            //  获取到skuImageList 集合
            List<SkuImage> skuImageList = skuImageMapper.selectList(new QueryWrapper<SkuImage>().eq("sku_id", skuId));
            //  报空指针  skuInfo = null
            skuInfo.setSkuImageList(skuImageList);
        }
        //  商品的名称，图片，价格 ，skuImageList 集合
        return skuInfo;
    }

    @Override
    @GmallCache(prefix = "CategoryViewByCategory3Id:")
    public BaseCategoryView getCategoryViewByCategory3Id(Long category3Id) {
        //  select * from v_category_view where id = 61;
        return baseCategoryViewMapper.selectById(category3Id);
    }

    @Override
    public BigDecimal getSkuPrice(Long skuId) {
        //  只要查询price
        //  SkuInfo skuInfo = skuInfoMapper.selectById(skuId);
        //  select price sku_info where id = skuId;
        QueryWrapper<SkuInfo> skuInfoQueryWrapper = new QueryWrapper<>();
        skuInfoQueryWrapper.select("price").eq("id",skuId);
        SkuInfo skuInfo = skuInfoMapper.selectOne(skuInfoQueryWrapper);
        if (skuInfo!=null){
            //  如果有数据，只要price字段，其他不要
            return skuInfo.getPrice();
        }
        //  如果没有，默认给0
        return new BigDecimal(0);
    }

    @Override
    @GmallCache(prefix = "SpuSaleAttrListCheckBySku:")
    public List<SpuSaleAttr> getSpuSaleAttrListCheckBySku(Long skuId, Long spuId) {
        //  调用mapper 层的方法
        return spuSaleAttrMapper.selectSpuSaleAttrListCheckBySku(skuId,spuId);
    }

    @Override
    @GmallCache(prefix = "SkuValueIdsMap:")
    public Map getSkuValueIdsMap(Long spuId) {
        //  声明一个map 集合
        Map<Object,Object> map = new HashMap<>();
        //  Map 可以转为Json  {"114|116":"44","114|117":"45"}
        //  页面获取到的是 114|116 获取到 skuId = 44
        //  map.put("114|116","44");
        //  将数据从数据库中获取出来！
        List<Map> mapList = skuSaleAttrValueMapper.selectSkuValueIdsMap(spuId);
        /*
        sku_id  value_ids
            44	114|116
            45	114|117
         */
        for (Map skuMap : mapList) {
            // 赋值操作
            map.put(skuMap.get("value_ids"),skuMap.get("sku_id"));
        }
        //  返回map集合
        return map;
    }

    @Override
    @GmallCache(prefix = "index:")
    public List<JSONObject> getCategoryList() {
        //  声明一个集合
        List<JSONObject> list = new ArrayList<>();
        /*
        1.  需要先获取到所有的分类数据

        2.  需要将查询到的数据进行分组操作！ 分组的条件：分别是category1Id, category2Id 获取到分类的名称

        3.  将获取到的分类数据的名称，以及分类的Id 装载到list 集合中！
         */
        List<BaseCategoryView> baseCategoryViewList = baseCategoryViewMapper.selectList(null);

        //  按照category1Id 进行分组
        //  map key = category1Id   value = List<BaseCategoryView>
        Map<Long, List<BaseCategoryView>> baseCategory1Map = baseCategoryViewList.stream().collect(Collectors.groupingBy(BaseCategoryView::getCategory1Id));

        //  声明一个index变量
        int index = 1;
        //  循环遍历这个map 集合 第一种迭代器
        //        Iterator<Map.Entry<Long, List<BaseCategoryView>>> iterator = baseCategory1Map.entrySet().iterator();
        //        while (iterator.hasNext()){
        //            Map.Entry<Long, List<BaseCategoryView>> entry = iterator.next();
        //            Long category1Id = entry.getKey();
        //            List<BaseCategoryView> baseCategoryViewList1 = entry.getValue();
        //        }

        //  第二种遍历方式
        for (Map.Entry<Long, List<BaseCategoryView>> entry : baseCategory1Map.entrySet()) {
            Long category1Id = entry.getKey();
            List<BaseCategoryView> baseCategoryViewList1 = entry.getValue();

            //  创建一个JsonObject 对象 一级分类对象
            JSONObject category1 = new JSONObject();
            category1.put("index",index);
            category1.put("categoryId",category1Id); // 一级分类Id
            category1.put("categoryName",baseCategoryViewList1.get(0).getCategory1Name()); // 一级分类名称

            //  index 遍历需要迭代
            index++;
            //  处理二级分类数据： key = category2Id value =
            Map<Long, List<BaseCategoryView>> baseCategory2Map = baseCategoryViewList1.stream().collect(Collectors.groupingBy(BaseCategoryView::getCategory2Id));
            //  要声明一个集合来存储一级分类下对应的二级分类名称！
            List<JSONObject> categoryChild2 = new ArrayList<>();
            //  遍历当前这个集合
            for (Map.Entry<Long, List<BaseCategoryView>> entry2 : baseCategory2Map.entrySet()) {
                Long category2Id = entry2.getKey();
                List<BaseCategoryView> baseCategoryViewList2 = entry2.getValue();
                //  二级分类对象
                JSONObject category2 = new JSONObject();
                category2.put("categoryId",category2Id); // 二级分类Id
                category2.put("categoryName",baseCategoryViewList2.get(0).getCategory2Name()); // 二级分类名称

                //  将每个二级分类对应的数据 添加到这个集合中
                categoryChild2.add(category2);

                //  声明一个集合来存储三级分类数据
                List<JSONObject> categoryChild3 = new ArrayList<>();
                //  获取三级分类数据
                baseCategoryViewList2.forEach((baseCategoryView) -> {
                    //  创建一个三级分类对象
                    JSONObject category3 = new JSONObject();
                    category3.put("categoryId",baseCategoryView.getCategory3Id());
                    category3.put("categoryName",baseCategoryView.getCategory3Name());

                    categoryChild3.add(category3);
                });
                //  将三级分类的集合数据添加到二级分类上！
                category2.put("categoryChild",categoryChild3);
            }
            //  将二级分类的集合数据添加到一级分类上！
            category1.put("categoryChild",categoryChild2);
            //  将所有的一级分类数据添加到集合中
            list.add(category1);
        }
        //  返回数据
        return list;
    }

    @Override
    public BaseTrademark getTrademarkByTmId(Long tmId) {
        //  select * from base_trademark where id = tmId;
        return baseTrademarkMapper.selectById(tmId);
    }

    @Override
    public List<BaseAttrInfo> getAttrList(Long skuId) {
        //  调用mapper层方法！
        return baseAttrInfoMapper.selectAttrList(skuId);
    }
}
