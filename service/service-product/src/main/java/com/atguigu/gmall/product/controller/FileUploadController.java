package com.atguigu.gmall.product.controller;

import com.atguigu.gmall.common.result.Result;
import io.minio.BucketExistsArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.XmlParserException;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.rmi.ServerException;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

/**
 * @author atguigu-mqx
 */
@RestController
@RequestMapping("admin/product")
public class FileUploadController {


    @Value("${minio.endpointUrl}")
    private String endpointUrl;

    @Value("${minio.accessKey}")
    public String accessKey;

    @Value("${minio.secreKey}")
    public String secreKey;

    @Value("${minio.bucketName}")
    public String bucketName;
//
//    //  文件上传 http://api.gmall.com/admin/product/fileUpload
//    //  文件上传的参数file!    springMVC 的文件上传！
    @PostMapping("fileUpload")
    public Result fileUpload(MultipartFile file) throws Exception {

        // 使用MinIO服务的URL，端口，Access key和Secret key创建一个MinioClient对象
        //  MinioClient minioClient = new MinioClient("https://play.min.io", "Q3AM3UQ867SPQQA43P2F", "zuf+tfteSlswRu7BJ86wekitnifILbZam1KYY3TG");
        MinioClient minioClient =
                MinioClient.builder()
                        .endpoint(endpointUrl)
                        .credentials(accessKey, secreKey)
                        .build();
        // 检查存储桶是否已经存在
        boolean isExist = minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build());
        if(isExist) {
            System.out.println("Bucket already exists.");
        } else {
            // 创建一个名为gmall的存储桶，用于存储图片数据！
            minioClient.makeBucket(MakeBucketArgs.builder().bucket(bucketName).build());
        }

        // 使用putObject上传一个文件到存储桶中。
        //  minioClient.putObject("asiatrip","asiaphotos.zip", "/home/user/Photos/asiaphotos.zip");
        //  文件名称 zly.jpg
        //  获取文件的扩展名 jpg
        String extName = FilenameUtils.getExtension(file.getOriginalFilename());
        //
        String fileName = System.currentTimeMillis()+ UUID.randomUUID().toString()+"."+extName;
        minioClient.putObject(
                PutObjectArgs.builder().bucket(bucketName).object(fileName).stream(
                        file.getInputStream(), file.getSize(), -1)
                        .contentType(file.getContentType()) // 不知道文件的类型就会下载！ 有这个会在浏览器中回显URL!
                        .build());

        //  回显：图片的全路径！
        //  http://39.99.159.121:9000/gmall/xxxxx;
        String url = endpointUrl+"/"+bucketName+"/"+fileName;
        System.out.println("url:\t"+url);
        //  文件上传之后的url 路径放入ok 中！
        return Result.ok(url);
    }
//    @PostMapping("fileUpload")
//    public Result fileUpload(MultipartFile file) {
//
//        String url = null;
//        try {
//            url = minioService.upload(file.getInputStream(), null, file.getOriginalFilename());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return Result.ok(url);
//    }

}
