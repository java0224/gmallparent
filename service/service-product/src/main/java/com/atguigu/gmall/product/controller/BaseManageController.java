package com.atguigu.gmall.product.controller;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.model.product.*;
import com.atguigu.gmall.product.service.ManageService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author atguigu-mqx
 */
@Api("后台管理接口")
@RestController //  @Controller + @ResponseBody
@RequestMapping("admin/product")
//@CrossOrigin
public class BaseManageController {

    //  控制器需要引入服务层
    @Autowired
    private ManageService manageService;

    //  获取一级分类的URL
    @GetMapping("getCategory1")
    public Result getCategory1(){
        //  调用服务层方法
        List<BaseCategory1> baseCategory1List = manageService.getBaseCategory1();
        return Result.ok(baseCategory1List);
    }

    //  根据一级分类Id 来获取二级分类数据
    @GetMapping("getCategory2/{category1Id}")
    public Result getCategory2(@PathVariable Long category1Id){
        //  调用服务层方法
        List<BaseCategory2> baseCategory2List = manageService.getBaseCategory2(category1Id);
        return Result.ok(baseCategory2List);
    }

    //  根据二级分类Id 来获取三级分类数据
    @GetMapping("getCategory3/{category2Id}")
    public Result getCategory3(@PathVariable Long category2Id){
        //  调用服务层方法
        List<BaseCategory3> baseCategory3List = manageService.getBaseCategory3(category2Id);
        return Result.ok(baseCategory3List);
    }

    //  通过分类Id 加载平台属性的时候
    @GetMapping("attrInfoList/{category1Id}/{category2Id}/{category3Id}")
    public Result attrInfoList(@PathVariable Long category1Id,
                               @PathVariable Long category2Id,
                               @PathVariable Long category3Id){
        //  调用服务层方法
        List<BaseAttrInfo> baseAttrInfoList = manageService.getBaseAttrInfoList(category1Id, category2Id, category3Id);

        return Result.ok(baseAttrInfoList);
    }

    //  http://api.gmall.com/admin/product/saveAttrInfo
    @PostMapping("saveAttrInfo")
    public Result saveAttrInfo(@RequestBody BaseAttrInfo baseAttrInfo){
        manageService.saveAttrInfo(baseAttrInfo);
        //  直接返回
        return Result.ok();
    }

    //  http://api.gmall.com/admin/product/getAttrValueList/{attrId}
    @GetMapping("getAttrValueList/{attrId}")
    public Result getAttrValueList(@PathVariable Long attrId){
        //  调用服务层方法
        //  List<BaseAttrValue> baseAttrValueList =  manageService.getAttrValueList(attrId);
        BaseAttrInfo baseAttrInfo =  manageService.getBaseAttrInfo(attrId);

        //  通过平台属性对象获取到平台属性值集合！
        return Result.ok(baseAttrInfo.getAttrValueList());
    }



}
