package com.atguigu.gmall.product.controller;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * @author atguigu-mqx
 */

public class CompletableFutureDemo {
    //  主入口：
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        //  一般不用：CompletableFuture completableFuture = new CompletableFuture();
        //        CompletableFuture<Void> completableFuture = CompletableFuture.runAsync(() -> {
        //            System.out.println("hello word");
        //        });
        //
        //        System.out.println(completableFuture.get());
        //
        //        CompletableFuture<Integer> integerCompletableFuture = CompletableFuture.supplyAsync(new Supplier<Integer>() {
        //            @Override
        //            public Integer get() {
        //                //  int i = 1/0;
        //                return 1024;
        //            }
        //        }).thenApply(new Function<Integer, Integer>() {
        //            @Override
        //            public Integer apply(Integer integer) {
        //                //  接收上一个方法，并返回自己的值！
        //                //  integer 是上一个的结果 1024
        //                System.out.println(integer +" #############");
        //                return integer*2;
        //            }
        //        }).whenCompleteAsync(new BiConsumer<Integer, Throwable>() {
        //            @Override
        //            public void accept(Integer integer, Throwable throwable) {
        //                System.out.println(integer+"--------------"); //2048
        //                System.out.println(throwable+"--------------");
        //            }
        //        }).exceptionally(new Function<Throwable, Integer>() {
        //            @Override
        //            public Integer apply(Throwable throwable) {
        //                System.out.println(throwable+"-==============");
        //                return 404;
        //            }
        //        });
        //
        //        System.out.println(integerCompletableFuture.get());

        //  并行：
        CompletableFuture<String> futureA = CompletableFuture.supplyAsync(() -> {
            return "hello:\t";
        });

        //  创建一个futureB线程，futureC 这两个线程依赖futureA 线程的结果！
        CompletableFuture<Void> futureB = futureA.thenAcceptAsync((s) -> {
            //  睡眠一会
            delay(3);
            //  直接打印
            printStr(s + "futureB");
        });

        CompletableFuture<Void> futureC = futureA.thenAcceptAsync((s) -> {
            //  睡眠一会
            delay(1);
            //  直接打印
            printStr(s + "futureC");
        });

        System.out.println(futureB.get());
        System.out.println(futureC.get());


    }

    private static void printStr(String s) {
        System.out.println(s);
    }

    private static void delay(int i) {
        try {
            Thread.sleep(i*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
