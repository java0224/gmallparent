package com.atguigu.gmall.product.controller;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.model.product.SkuInfo;
import com.atguigu.gmall.product.service.ManageService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author atguigu-mqx
 */
@RestController
@RequestMapping("admin/product")
public class SkuManageController {

    @Autowired
    private ManageService manageService;

    //  http://localhost/admin/product/saveSkuInfo
    //  将前端数据转换为JavaObject
    @PostMapping("saveSkuInfo")
    public Result saveSkuInfo(@RequestBody SkuInfo skuInfo){
        //  调用方法
        manageService.saveSkuInfo(skuInfo);
        return Result.ok();
    }

    //  http://api.gmall.com/admin/product/list/{page}/{limit}
    //  查询skuInfo 列表！
    @GetMapping("list/{page}/{limit}")
    public Result getSkuInfoList(@PathVariable Long page,
                                 @PathVariable Long limit){
        //  构建一个Page 对象
        Page<SkuInfo> skuInfoPage = new Page<>(page, limit);

        //  调用服务层的方法
        IPage skuIpage =  manageService.getSkuInfoList(skuInfoPage);

        //  返回数据
        return Result.ok(skuIpage);
    }


    //  上架
    //  http://api.gmall.com/admin/product/onSale/{skuId}
    @GetMapping("onSale/{skuId}")
    public Result onSale(@PathVariable Long skuId){
        //  调用服务层方法
        manageService.onSale(skuId);

        return Result.ok();
    }

    //  下架
    //  http://api.gmall.com/admin/product/cancelSale/{skuId}
    @GetMapping("cancelSale/{skuId}")
    public Result cancelSale(@PathVariable Long skuId){
        //  调用服务层方法
        manageService.cancelSale(skuId);

        return Result.ok();
    }
}
