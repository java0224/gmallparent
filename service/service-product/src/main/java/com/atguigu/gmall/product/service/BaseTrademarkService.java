package com.atguigu.gmall.product.service;

import com.atguigu.gmall.model.product.BaseTrademark;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author atguigu-mqx
 */
public interface BaseTrademarkService extends IService<BaseTrademark> {

    /**
     * 获取到品牌数据
     * @param baseTrademarkPage
     * @return
     */
    IPage<BaseTrademark> getBaseTradeMarekList(Page<BaseTrademark> baseTrademarkPage);

    //  void save(BaseTrademark baseTrademark);
}
