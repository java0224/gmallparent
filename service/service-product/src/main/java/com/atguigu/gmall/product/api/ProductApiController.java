package com.atguigu.gmall.product.api;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.model.product.*;
import com.atguigu.gmall.product.service.ManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @author atguigu-mqx
 */
@RestController
@RequestMapping("api/product")
public class ProductApiController {

    @Autowired
    private ManageService manageService;

    //  用户通过网关 -- web-all --feign{api/item/{skuId}}--> service-item --feign{api/product/inner/getSkuInfo/{skuId}}--> service-product
    //  这个方法只获取到 skuName ,price ,defaultImage ,skuImageList 等数据。
    //  需要将刚刚的方法 getSkuInfo 发布到feign 上！
    @GetMapping("inner/getSkuInfo/{skuId}")
    public SkuInfo getSkuInfo(@PathVariable Long skuId){
        //  返回数据！
        return manageService.getSkuInfo(skuId);
    }

    //  将获取到的分类数据 发送给service-item!
    @GetMapping("inner/getCategoryView/{category3Id}")
    public BaseCategoryView getCategoryView(@PathVariable Long category3Id){
        //  返回数据
        return manageService.getCategoryViewByCategory3Id(category3Id);
    }

    //  将商品的价格发送给item！
    @GetMapping("inner/getSkuPrice/{skuId}")
    public BigDecimal getSkuPrice(@PathVariable Long skuId){
        //  返回数据
        return manageService.getSkuPrice(skuId);
    }

    //  回显销售属性 + 销售属性值 + 锁定功能
    @GetMapping("inner/getSpuSaleAttrListCheckBySku/{skuId}/{spuId}")
    public List<SpuSaleAttr> getSpuSaleAttrListCheckBySku(@PathVariable Long skuId,
                                                          @PathVariable Long spuId){
        //  调用服务层方法
        return manageService.getSpuSaleAttrListCheckBySku(skuId,spuId);
    }

    //  获取到销售属性值Id 与 skuId 组成的map 集合！
    //  这json 字符串，可以在service-item 这个微服务进行转换！
    @GetMapping("inner/getSkuValueIdsMap/{spuId}")
    public Map getSkuValueIdsMap(@PathVariable Long spuId){
        //  返回数据
        return manageService.getSkuValueIdsMap(spuId);
    }

    //  获取到首页分类数据
    @GetMapping("getBaseCategoryList")
    public Result getBaseCategoryList(){
        //  调用服务层的方法
        List<JSONObject> categoryList = manageService.getCategoryList();
        return Result.ok(categoryList);
    }


    //  根据品牌Id 来获取品牌数据
    @GetMapping("inner/getTrademark/{tmId}")
    public BaseTrademark getTrademark(@PathVariable Long tmId){
        //  获取数据
        return manageService.getTrademarkByTmId(tmId);
    }

    //  定义一个控制器
    @GetMapping("inner/getAttrList/{skuId}")
    public List<BaseAttrInfo> getAttrList(@PathVariable Long skuId){
        //  调用服务层方法！
        return manageService.getAttrList(skuId);
    }

}
