package com.atguigu.gmall.product.controller;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.model.product.BaseTrademark;
import com.atguigu.gmall.product.service.BaseTrademarkService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author atguigu-mqx
 */
@RestController
@RequestMapping("admin/product/baseTrademark")
public class TradeMarkController {

    //  调用服务层的方法
    @Autowired
    private BaseTrademarkService baseTrademarkService;
    //  获取到所有的品牌数据
    //  http://api.gmall.com/admin/product/baseTrademark/{page}/{limit}
    @GetMapping("{page}/{limit}")
    public Result getBaseTradeMarekList(@PathVariable Long page,
                                        @PathVariable Long limit){
        //  创建一个page对象
        Page<BaseTrademark> baseTrademarkPage = new Page<>(page,limit);
        IPage<BaseTrademark> iPage = baseTrademarkService.getBaseTradeMarekList(baseTrademarkPage);
        return Result.ok(iPage);
    }

    //  http://api.gmall.com/admin/product/baseTrademark/save
    //  传递的数据是Json {tmName: "锤子。", logoUrl: "/static/default.jpg"}
    //  Json --- >JavaObject
    @PostMapping("save")
    public Result save(@RequestBody BaseTrademark baseTrademark){
        //  调用服务层的方法
        baseTrademarkService.save(baseTrademark);
        return Result.ok();
    }


    //  http://api.gmall.com/admin/product/baseTrademark/get/{id}
    @GetMapping("get/{id}")
    public Result getById(@PathVariable Long id){
        //  获取到品牌对象
        BaseTrademark baseTrademark = baseTrademarkService.getById(id);
        return Result.ok(baseTrademark);
    }

    //  http://api.gmall.com/admin/product/baseTrademark/update
    @PutMapping("update")
    public Result updateTrade(@RequestBody BaseTrademark baseTrademark){

        //  调用修改的方法！
        baseTrademarkService.updateById(baseTrademark);
        //  返回
        return Result.ok();
    }

    //  http://api.gmall.com/admin/product/baseTrademark/remove/{id}
    @DeleteMapping("remove/{id}")
    public Result removeByid(@PathVariable Long id){
        //  调用删除方法
        baseTrademarkService.removeById(id);
        return Result.ok();
    }

    //  http://localhost/admin/product/baseTrademark/getTrademarkList
    @GetMapping("getTrademarkList")
    public Result getTrademarkList(){
        //  获取到品牌的集合数据
        List<BaseTrademark> list = baseTrademarkService.list(); // = baseTrademarkService.list(null); xxxMapper.selectList(null)
        //  baseTrademarkService.list(null);
        return Result.ok(list);
    }

}
