package com.atguigu.gmall.product.controller;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.model.product.BaseSaleAttr;
import com.atguigu.gmall.model.product.SpuImage;
import com.atguigu.gmall.model.product.SpuInfo;
import com.atguigu.gmall.model.product.SpuSaleAttr;
import com.atguigu.gmall.product.service.ManageService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author atguigu-mqx
 *
 */
@RestController
@RequestMapping("admin/product")
public class SpuManageController {

    //  注入服务层
    @Autowired
    private ManageService manageService;

    //  http://api.gmall.com/admin/product/{page}/{limit}?category3Id=61
    //  http://api.gmall.com/admin/product/1/10?category3Id=61
    @GetMapping("{page}/{limit}")
    public Result getSpuInfoList(@PathVariable Long page,
                                 @PathVariable Long limit,
                                 //HttpServletRequest request
                                 SpuInfo spuInfo){
        //        String category3Id = request.getParameter("category3Id");
        //  myatis-plus 的分页查询数据 IPage Page
        Page<SpuInfo> spuInfoPage = new Page<SpuInfo>(page,limit);
        //  调用服务层方法
        IPage<SpuInfo> infoIPage = manageService.getSpuInfoList(spuInfoPage,spuInfo);
        //  返回数据
        return Result.ok(infoIPage);
    }

    //  http://localhost/admin/product/baseSaleAttrList
    @GetMapping("baseSaleAttrList")
    public Result baseSaleAttrList(){
        List<BaseSaleAttr> baseSaleAttrList =  manageService.getBaseSaleAttrList();
        //  返回数据
        return Result.ok(baseSaleAttrList);
    }

    //  http://localhost/admin/product/saveSpuInfo
    @PostMapping("saveSpuInfo")
    public Result saveSpuInfo(@RequestBody SpuInfo spuInfo){
        manageService.saveSpuInfo(spuInfo);
        //  默认返回
        return Result.ok();
    }

    //  http://localhost/admin/product/spuImageList/27
    //  表示根据spuId 获取到spuImage 集合！
    @GetMapping("spuImageList/{spuId}")
    public Result getSpuImageList(@PathVariable Long spuId){

        //  需要调用服务层的方法
        List<SpuImage> spuImageList =  manageService.getSpuImageList(spuId);
        return Result.ok(spuImageList);
    }

    //  回显销售属性数据
    //  http://localhost/admin/product/spuSaleAttrList/27
    @GetMapping("spuSaleAttrList/{spuId}")
    public Result getSpuSaleAttrList(@PathVariable Long spuId){

        //  调用服务层方法 map集合{key value}? List销售属性  销售属性值-列表
        List<SpuSaleAttr> spuSaleAttrList = manageService.getSpuSaleAttrList(spuId);

        return Result.ok(spuSaleAttrList);
    }

}

