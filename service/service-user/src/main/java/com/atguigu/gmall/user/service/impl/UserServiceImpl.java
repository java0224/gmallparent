package com.atguigu.gmall.user.service.impl;

import com.atguigu.gmall.common.util.MD5;
import com.atguigu.gmall.model.user.UserInfo;
import com.atguigu.gmall.user.mapper.UserInfoMapper;
import com.atguigu.gmall.user.service.UserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author 花鸟の虫鱼
 * @CreateDate 21/8/19 11:20
 * @Version 1.0
 * @Description TODO
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserInfoMapper userInfoMapper;

    @Override
    public UserInfo login(UserInfo userInfo) {
        //  select * from user_info where username = ? and pwassword = ?; 这个语句执行完成之后，只能有一个对象出现！
        QueryWrapper<UserInfo> userInfoQueryWrapper = new QueryWrapper<>();
        userInfoQueryWrapper.eq("login_name",userInfo.getLoginName());
        //  数据库中密码 [96e79218965eb72c92a549dd5a330112]
        //  对用户输入的密码进行加密，用加密之后的密码去数据库进行查询！
        String passwd = userInfo.getPasswd();
        //  MD5
        String newPwd = DigestUtils.md5DigestAsHex(passwd.getBytes());
        userInfoQueryWrapper.eq("passwd",newPwd);
        UserInfo info = userInfoMapper.selectOne(userInfoQueryWrapper);
        //  判断是否登录成功！
        if (info!=null){
            return info;
        }
        return null;
    }

    public static void main(String[] args) {
        //  spring 提供的
        System.out.println(DigestUtils.md5DigestAsHex("111111".getBytes()));
        System.out.println(MD5.encrypt("111111"));
    }
}
