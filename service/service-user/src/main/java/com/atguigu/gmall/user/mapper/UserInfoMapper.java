package com.atguigu.gmall.user.mapper;


import com.atguigu.gmall.model.user.UserInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author 花鸟の虫鱼
 * @CreateDate 21/8/19 11:19
 * @Version 1.0
 * @Description TODO
 */
@Mapper
public interface UserInfoMapper extends BaseMapper<UserInfo> {
}