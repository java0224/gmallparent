package com.atguigu.gmall.user.service;

import com.atguigu.gmall.model.user.UserInfo;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author 花鸟の虫鱼
 * @CreateDate 21/8/19 11:19
 * @Version 1.0
 * @Description TODO
 */
public interface UserService {
    //  登录接口
    UserInfo login(UserInfo userInfo);
}
