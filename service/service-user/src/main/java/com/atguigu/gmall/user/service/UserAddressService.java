package com.atguigu.gmall.user.service;

import com.atguigu.gmall.model.user.UserAddress;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author 花鸟の虫鱼
 * @CreateDate 21/8/24 17:21
 * @Version 1.0
 * @Description TODO
 */
public interface UserAddressService {

    /**
     * 根据用户Id 查询收货地址列表
     * @param userId
     * @return
     */
    List<UserAddress> findUserAddressListByUserId(Long userId);
}
