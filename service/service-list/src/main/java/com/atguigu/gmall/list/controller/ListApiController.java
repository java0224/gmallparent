package com.atguigu.gmall.list.controller;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.list.service.SearchService;
import com.atguigu.gmall.model.list.Goods;
import com.atguigu.gmall.model.list.SearchParam;
import com.atguigu.gmall.model.list.SearchResponseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.web.bind.annotation.*;

/**
 * @author atguigu-mqx
 */
@RestController
@RequestMapping("api/list")
public class ListApiController {

    //  操作es 的客户端
    @Autowired
    private ElasticsearchRestTemplate restTemplate;

    @Autowired
    private SearchService searchService;

    //  自定义一个映射器 ，访问这个映射器就能够自动在es 中创建mapping 索引库！
    @GetMapping("inner/createIndex")
    public Result createIndex(){

        //  调用客户端方法 , 方法有中划线表示已过期，但是可以使用！
        //  版本6.8.1 自定义实体类通过这个方式创建， 版本7.8.0 这个版本的方法已过时。
        //  7.8.0 它能够自动创建！ 启动项目的时候能够自动创建！ {跟其他的接口有关系}
        restTemplate.createIndex(Goods.class); //  创建对应的Index！
        restTemplate.putMapping(Goods.class);  //  创建对应的mapping 映射！
        return Result.ok();
    }

    //  商品上架：
    @GetMapping("inner/upperGoods/{skuId}")
    public Result upperGoods(@PathVariable Long skuId){
        searchService.upperGoods(skuId);
        return Result.ok();
    }

    //  商品下架：
    @GetMapping("inner/lowerGoods/{skuId}")
    public Result lowerGoods(@PathVariable Long skuId){
        searchService.lowerGoods(skuId);
        return Result.ok();
    }

    //  热度排名
    @GetMapping("inner/incrHotScore/{skuId}")
    public Result incrHotScore (@PathVariable Long skuId){
        //  调用排名方法
        searchService.incrHotScore(skuId);

        return Result.ok();
    }

    //  返回值数据：Result，通常给web-all要做提供数据的时候，例如内部数据接口，可能不用
    //  @RequetBody
    @PostMapping
    public Result search(@RequestBody SearchParam searchParam) {
        //  调用服务层方法
        SearchResponseVo searchResponse = searchService.search(searchParam);
        //  返回数据
        return Result.ok(searchResponse);
    }
}
