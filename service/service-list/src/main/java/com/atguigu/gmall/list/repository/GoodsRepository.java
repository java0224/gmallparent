package com.atguigu.gmall.list.repository;

import com.atguigu.gmall.model.list.Goods;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * @author atguigu-mqx
 */
//  T:Goods ID:实体类Id 的数据类型
public interface GoodsRepository extends ElasticsearchRepository<Goods, Long> {
}
