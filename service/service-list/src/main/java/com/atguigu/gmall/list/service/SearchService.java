package com.atguigu.gmall.list.service;

import com.atguigu.gmall.model.list.SearchParam;
import com.atguigu.gmall.model.list.SearchResponseVo;

/**
 * @author atguigu-mqx
 */
public interface SearchService {

    //  编写商品上架， 参数skuId
    void upperGoods(Long skuId);

    //  编写商品下架，参数skuId
    void lowerGoods(Long skuId);

    //  商品的热度排名
    void incrHotScore(Long skuId);

    //  编写查询数接口
    SearchResponseVo search(SearchParam searchParam);
}
