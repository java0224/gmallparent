package com.atguigu.gmall.list.receiver;

import com.atguigu.gmall.common.constant.MqConst;
import com.atguigu.gmall.list.service.SearchService;
import com.rabbitmq.client.Channel;
import lombok.SneakyThrows;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author atguigu-mqx
 */
@Component
public class GoodsReceiver {

    @Autowired
    private SearchService searchService;

    //  监听消息
    @SneakyThrows
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = MqConst.QUEUE_GOODS_UPPER,durable = "true",autoDelete = "false"),
            exchange = @Exchange(value = MqConst.EXCHANGE_DIRECT_GOODS),
            key = {MqConst.ROUTING_GOODS_UPPER}
    ))
    public void upperGoodsToEs(Long skuId, Message message, Channel channel){
        try {
            //  先判断
            if (skuId!=null){
                searchService.upperGoods(skuId);
            }
        } catch (Exception e) {
            //  记录日志...... 谁没有上架成功！ 记录表 table！
            e.printStackTrace();
        }
        //  手动确认！
        channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);

    }

    //  监听消息
    @SneakyThrows
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = MqConst.QUEUE_GOODS_LOWER,durable = "true",autoDelete = "false"),
            exchange = @Exchange(value = MqConst.EXCHANGE_DIRECT_GOODS),
            key = {MqConst.ROUTING_GOODS_LOWER}
    ))
    public void lowerGoodsToEs(Long skuId, Message message, Channel channel){
        try {
            //  先判断
            if (skuId!=null){
                searchService.lowerGoods(skuId);
            }
        } catch (Exception e) {
            //  记录日志...... 谁没有上架成功！ 记录表 table！
            e.printStackTrace();
        }
        //  手动确认！
        channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);

    }
}
