package com.atguigu.gmall.task.scheduled;

import com.atguigu.gmall.common.service.RabbitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author 花鸟の虫鱼
 * @CreateDate 21/8/30 15:25
 * @Version 1.0
 * @Description TODO
 */
@Component
@EnableScheduling
public class ScheduledTask {
    @Autowired
    private RabbitService rabbitService;

    @Scheduled(cron = "0/3 * * * * ?")
    public void testTask() {
        System.out.println("人太多了，今天就到这了···");
    //this.rabbitService.sendMessage(MqConst.EXCHANGE_DIRECT_TASK, MqConst.ROUTING_TASK_1, "来人了，接客吧姑娘们···");
    }
}
