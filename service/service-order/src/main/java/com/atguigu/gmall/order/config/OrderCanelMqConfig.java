package com.atguigu.gmall.order.config;

import com.atguigu.gmall.common.constant.MqConst;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.CustomExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author 花鸟の虫鱼
 * @CreateDate 21/8/26 22:18
 * @Version 1.0
 * @Description TODO
 */
@Configuration
public class OrderCanelMqConfig {

    //  创建队列
    @Bean
    public Queue delayQueue(){
        //  创建并返回
        return new Queue(MqConst.QUEUE_ORDER_CANCEL,true,false,false);
    }

    //  创建一个交换机
    @Bean
    public CustomExchange delayExchange(){
        //  设置交换机类型等参数
        HashMap<String, Object> map = new HashMap<>();
        map.put("x-delayed-type","direct");
        //  创建对象并返回
        return new CustomExchange(MqConst.EXCHANGE_DIRECT_ORDER_CANCEL,"x-delayed-message",true,false,map);
    }

    //  定义一个绑定关系！
    @Bean
    public Binding delayBinding(){
        //  return BindingBuilder.bind(queue2()).to(exchange()).with(routing_dead_2);
        //  特殊的返回
        return BindingBuilder.bind(delayQueue()).to(delayExchange()).with(MqConst.ROUTING_ORDER_CANCEL).noargs();
    }
}
