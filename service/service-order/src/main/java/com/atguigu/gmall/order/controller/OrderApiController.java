package com.atguigu.gmall.order.controller;


import com.alibaba.fastjson.JSON;
import com.atguigu.gmall.cart.client.CartFeignClient;
import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.common.util.AuthContextHolder;
import com.atguigu.gmall.model.cart.CartInfo;
import com.atguigu.gmall.model.order.OrderDetail;
import com.atguigu.gmall.model.order.OrderInfo;
import com.atguigu.gmall.model.user.UserAddress;
import com.atguigu.gmall.order.service.OrderService;
import com.atguigu.gmall.product.client.ProductFeignClient;
import com.atguigu.gmall.user.client.UserFeignClient;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author 花鸟の虫鱼
 * @CreateDate 21/8/24 18:35
 * @Version 1.0
 * @Description TODO
 */
@RestController
@RequestMapping("api/order")
public class OrderApiController {

    @Autowired
    private UserFeignClient userFeignClient;

    @Autowired
    private CartFeignClient cartFeignClient;

    @Autowired
    private OrderService orderService;

    @Autowired
    private ProductFeignClient productFeignClient;

    @Autowired
    private ThreadPoolExecutor threadPoolExecutor;

    //  映射路径：给web-all 使用！ web-all 页面渲染的！
    //  需要在后台存储 detailArrayList ， userAddressList, totalNum, totalAmount
    @GetMapping("auth/trade")
    public Result trade(HttpServletRequest request) {

        //  创建一个map
        HashMap<String, Object> map = new HashMap<>();

        //  获取用户Id
        String userId = AuthContextHolder.getUserId(request);

        //  获取用户的收货地址列表
        List<UserAddress> userAddressList = userFeignClient.findUserAddressListByUserId(userId);

        //  获取送货清单！ 只存储选中的商品！
        List<CartInfo> cartCheckedList = cartFeignClient.getCartCheckedList(userId);

        //  定义一个变量：
        int totalNum = 0;

        //  循环遍历：
        List<OrderDetail> detailArrayList = new ArrayList<>();
        for (CartInfo cartInfo : cartCheckedList) {
            OrderDetail orderDetail = new OrderDetail();
            orderDetail.setSkuId(cartInfo.getSkuId());
            orderDetail.setSkuName(cartInfo.getSkuName());
            orderDetail.setImgUrl(cartInfo.getImgUrl());
            orderDetail.setOrderPrice(cartInfo.getSkuPrice());
            orderDetail.setSkuNum(cartInfo.getSkuNum());
            orderDetail.setCreateTime(new Date());

            totalNum += orderDetail.getSkuNum();
            detailArrayList.add(orderDetail);
        }
        //  介绍orderInfo;
        OrderInfo orderInfo = new OrderInfo();
        orderInfo.setOrderDetailList(detailArrayList);
        //  调用这个方法会自动计算总金额！
        orderInfo.sumTotalAmount();

        //  保存数据
        map.put("userAddressList", userAddressList);
        map.put("detailArrayList", detailArrayList);
        //  商品数量有两种算法： 一种直接获取集合的长度！ 第二种：将每个skuId 的个数进行相加
        //  map.put("totalNum",detailArrayList.size());
        map.put("totalNum", totalNum);
        map.put("totalAmount", orderInfo.getTotalAmount());

        //  存储流水号
        map.put("tradeNo", orderService.getTradeNo(userId));

        //  返回数据
        return Result.ok(map);
    }

    //  保存订单： http://api.gmall.com/api/order/auth/submitOrder?tradeNo=null
    //  后台接收数据
    @PostMapping("auth/submitOrder")
    public Result submitOrder(@RequestBody OrderInfo orderInfo, HttpServletRequest request) {
        //  存储userId
        String userId = AuthContextHolder.getUserId(request);
        orderInfo.setUserId(Long.parseLong(userId));

        //  获取到页面的流水号
        String tradeNo = request.getParameter("tradeNo");
        //  比较
        Boolean flag = orderService.checkTradeNo(tradeNo, userId);

        //  判断
        //        if(flag){
        //            //  treu ： 可以提交
        //        }else {
        //            //  false:  不能提交！
        //        }
        if (!flag) {
            //  不正常，不能提交的！
            return Result.fail().message("不能重复提交订单!");
        }
        //  删除流水号
        orderService.deleteTradeNo(userId);

        //  多线程：
        //  CompletableFuture.runAsync();   //  没有返回值！ 将错误信息存储到一个集合中！
        List<String> errorList = new ArrayList<>();
        //  CompletableFuture.supplyAsync();//  有返回值！
        //  声明一个CompletableFuture 集合
        List<CompletableFuture> completableFutureList = new ArrayList<>();
        List<OrderDetail> orderDetailList = orderInfo.getOrderDetailList();
        //  orderDetailList 这个集合长度 ： 如果是3 ；
        for (OrderDetail orderDetail : orderDetailList) {
            CompletableFuture<Void> stockCompletableFuture = CompletableFuture.runAsync(() -> {
                boolean result = orderService.checkStock(orderDetail.getSkuId(), orderDetail.getSkuNum());
                if (!result) {
                    //  不正常，不能提交的！
                    //  log 日志。。。。。  发送短信！通知赶紧补货！
                    // Result.fail().message(orderDetail.getSkuName()+"库存不足!");
                    errorList.add(orderDetail.getSkuName() + "库存不足!");
                }
            }, threadPoolExecutor);
            //  将验证库存不足的线程添加到这个集合中！
            completableFutureList.add(stockCompletableFuture);

            //  验证价格：
            CompletableFuture<Void> priceCompletableFuture = CompletableFuture.runAsync(() -> {
                //  验证价格： 下单的价格与商品的实时价格[skuInfo.getPrice()]比较
                //  先获取实时价格
                BigDecimal skuPrice = productFeignClient.getSkuPrice(orderDetail.getSkuId());
                //  价格比较
                if (orderDetail.getOrderPrice().compareTo(skuPrice) != 0) {
                    //  有了价格变动，需要更改价格! 先改购物车价格！
                    //  是否只改缓存就可以了? 购物车页面展示价格： skuPrice!
                    cartFeignClient.loadCartToCache(userId);
                    errorList.add(orderDetail.getSkuName() + "价格有变动!");
                }
            }, threadPoolExecutor);
            //  将验证价格的线程添加到这个集合
            completableFutureList.add(priceCompletableFuture);
        }

        //  多任务组合：
        CompletableFuture.allOf(completableFutureList.toArray(new CompletableFuture[completableFutureList.size()])).join();
        //  判断是否要给页面提示信息！
        if (errorList.size() > 0) {
            return Result.fail().message(StringUtils.join(errorList, ","));
        }

        //        List<CartInfo> cartInfos = new ArrayList<>();
        //        for (OrderDetail orderDetail : orderDetailList) {
        //            CartInfo cartInfo = new CartInfo();
        //            cartInfo.setSkuPrice(orderDetail.getOrderPrice());
        //            cartInfos.add(cartInfo);
        //        }
        //  验证库存：
        //        List<OrderDetail> orderDetailList = orderInfo.getOrderDetailList();
        //        for (OrderDetail orderDetail : orderDetailList) {
        //            //  获取验证结果
        //            boolean result = orderService.checkStock(orderDetail.getSkuId(), orderDetail.getSkuNum());
        //            if (!result){
        //                //  不正常，不能提交的！
        //                //  log 日志。。。。。  发送短信！通知赶紧补货！
        //                return Result.fail().message(orderDetail.getSkuName()+"库存不足!");
        //            }
        //
        //            //  验证价格： 下单的价格与商品的实时价格[skuInfo.getPrice()]比较
        //            //  先获取实时价格
        //            BigDecimal skuPrice = productFeignClient.getSkuPrice(orderDetail.getSkuId());
        //            //  价格比较
        //            if(orderDetail.getOrderPrice().compareTo(skuPrice)!=0){
        //                //  有了价格变动，需要更改价格! 先改购物车价格！
        //                //  是否只改缓存就可以了? 购物车页面展示价格： skuPrice!
        //                cartFeignClient.loadCartToCache(userId);
        //                return Result.fail().message(orderDetail.getSkuName()+"价格有变动!");
        //            }
        //        }

        //  调用服务层方法
        Long orderId = orderService.saveOrderInfo(orderInfo);

        //  返回orderId
        return Result.ok(orderId);
    }

    //  提供数据给web-all
    @GetMapping("inner/getOrderInfo/{orderId}")
    public OrderInfo getOrderInfo(@PathVariable Long orderId) {
        //  返回数据
        return orderService.getOrderInfo(orderId);
    }

    //
    //
    @PostMapping("orderSplit")
    public String orderSplit(HttpServletRequest request) {
        //  获取url后面的参数
        String orderId = request.getParameter("orderId");
        String wareSKuMap = request.getParameter("wareSkuMap");

        //  获取子订单集合 207,208
        List<OrderInfo> subOrderInfoList = orderService.orderSplit(orderId, wareSKuMap);
        List<Map> list = new ArrayList<>();
        //  循环遍历
        for (OrderInfo orderInfo : subOrderInfoList) {
            //  orderInfo ---> 转换Map
            Map map = orderService.initWareOrder(orderInfo);
            list.add(map);
        }
        //  返回子订单集合
        return JSON.toJSONString(list);
    }
}