package com.atguigu.gmall.order.mapper;

import com.atguigu.gmall.model.order.OrderDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author 花鸟の虫鱼
 * @CreateDate 21/8/24 18:58
 * @Version 1.0
 * @Description TODO
 */
@Mapper
public interface OrderDetailMapper extends BaseMapper<OrderDetail> {
}
