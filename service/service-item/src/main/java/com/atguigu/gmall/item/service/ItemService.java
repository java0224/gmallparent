package com.atguigu.gmall.item.service;

import java.util.Map;

/**
 * @author atguigu-mqx
 */
public interface ItemService {

    //  定义数据接口： Thymeleaf 渲染数据时，可以在后台使用存储Map ,返回一个map
    //  页面渲染的时候：将页面渲染的key 放入map 中！ 数据汇总 数据是从service-product 中来！
    Map<String,Object> getItemById(Long skuId);




}
