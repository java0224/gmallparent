package com.atguigu.gmall.cart.mapper;

import com.atguigu.gmall.model.cart.CartInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author 花鸟の虫鱼
 * @CreateDate 21/8/20 20:23
 * @Version 1.0
 * @Description TODO
 */
@Mapper
public interface CartInfoMapper extends BaseMapper<CartInfo> {
}
