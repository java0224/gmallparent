package com.atguigu.gmall.cart.controller;

import com.atguigu.gmall.cart.service.CartService;
import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.common.util.AuthContextHolder;
import com.atguigu.gmall.model.cart.CarInfoVo;
import com.atguigu.gmall.model.cart.CartInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author atguigu-mqx
 */
@RestController
@RequestMapping("api/cart")
public class CartApiController {

    @Autowired
    private CartService cartService;

    @RequestMapping("addToCart/{skuId}/{skuNum}")
    public Result addToCart(@PathVariable Long skuId,
                            @PathVariable Integer skuNum,
                            HttpServletRequest request){
        //  想要获取用户Id！ 将用户Id 添加到请求头中！
        //  userId 是属于登录的用户Id
        String userId = AuthContextHolder.getUserId(request);
        //  添加购物车的时候， 可以是登录的时候添加，也可以是未登录的时候添加{临时用户Id}
        //  获取临时用户Id： 在添加购物车的时候，先判断用户是否登录，
        //  再判断cookie中是否有临时用户Id，如果cookie中没有临时用户Id，则生产一个临时用户Id 保存到cookie中！
        //  判断的是：未登录情况下添加的购物车！
        if (StringUtils.isEmpty(userId)){
            userId = AuthContextHolder.getUserTempId(request);
        }
        //  调用服务层方法
        cartService.addToCart(skuId,userId,skuNum);

        //  返回result
        return Result.ok();
    }

    //  查看购物车列表控制器
    @GetMapping("cartList")
    public Result cartList(HttpServletRequest request){
        //  获取登录用户Id ，再获取临时用户Id
        String userId = AuthContextHolder.getUserId(request);
        String userTempId = AuthContextHolder.getUserTempId(request);
        //  情况一：只有userId, 情况二：只有userTempId,情况三：两个都有{合并购物车}
        List<CartInfo> cartInfoList = cartService.getCartList(userId,userTempId);
        //  将这个购物车集合列表 放入result.ok(cartList);
        return Result.ok(cartInfoList);
    }

    //  获取商品的选中状态！
    @GetMapping("checkCart/{skuId}/{isChecked}")
    public Result checkCart(@PathVariable Long skuId ,
                            @PathVariable Integer isChecked,
                            HttpServletRequest request){
        //  跟用户Id 有关系！
        String userId = AuthContextHolder.getUserId(request);
        //  获取临时用户Id
        if (StringUtils.isEmpty(userId)){
            userId = AuthContextHolder.getUserTempId(request);
        }
        //  调用服务层方法
        cartService.checkCart(skuId,isChecked,userId);
        return Result.ok();
    }

    //  删除购物车：
    @DeleteMapping("/deleteCart/{skuId}")
    public Result deleteCart(@PathVariable Long skuId,
                             HttpServletRequest request){
        //  删除购物车，在登录，未登录的情况下都可以操作！
        //  跟用户Id 有关系！
        String userId = AuthContextHolder.getUserId(request);
        //  获取临时用户Id
        if (StringUtils.isEmpty(userId)){
            userId = AuthContextHolder.getUserTempId(request);
        }
        //  调用删除购物车数据的方法
        cartService.deleteCart(skuId,userId);
        return Result.ok();

    }

    //  获取送货清单！
    @GetMapping("getCartCheckedList/{userId}")
    public List<CartInfo> getCartCheckedList(@PathVariable String userId){
        //  获取送货清单！
        List<CartInfo> cartCheckedList = cartService.getCartCheckedList(userId);
        //  返回数据！
        return cartCheckedList;
    }

    //  定义一个接口：
    @GetMapping("loadCartCache/{userId}")
    public Result loadCartToCache(@PathVariable("userId") String userId) {
//        cartService.loadCartCache(userId);
        cartService.loadCartToCache(userId);
        return Result.ok();
    }
}
