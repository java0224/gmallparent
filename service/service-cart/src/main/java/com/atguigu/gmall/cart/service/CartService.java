package com.atguigu.gmall.cart.service;

import com.atguigu.gmall.model.cart.CartInfo;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author 花鸟の虫鱼
 * @CreateDate 21/8/20 20:25
 * @Version 1.0
 * @Description TODO
 */
public interface CartService {

    //  添加购物车数据接口！
    void addToCart(Long skuId,String userId,Integer skuNum);

    //  查看购物车列表
    List<CartInfo> getCartList(String userId, String userTempId);

    //  改变购物项的选中状态
    void checkCart(Long skuId, Integer isChecked, String userId);

    //  删除购物车
    void deleteCart(Long skuId, String userId);

    //  根据用户Id 来获取到购物车列表
    List<CartInfo> getCartCheckedList(String userId);

    //  根据用户Id 来获取购物车列表
    List<CartInfo> loadCartToCache(String userId);
}
