package com.atguigu.gmall.all.controller;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.item.client.ItemFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

/**
 * @author atguigu-mqx
 */
@Controller
public class ItemController {

    @Autowired
    private ItemFeignClient itemFeignClient;

    //  http://item.gmall.com/44.html
    //  http://item.gmall.com/45.html
    @RequestMapping("{skuId}.html")
    public String getItem(@PathVariable Long skuId, Model model){
        /*
        调用服务层
        Map<String, Object> map = itemService.getItemById(skuId);
        返回result
        return Result.ok(map);
         */
        Result<Map> result = itemFeignClient.getItem(skuId);

        //  result.getData(); 获取到的就是Map 集合
        //  将数据保存到作用域中
        model.addAllAttributes(result.getData());

        //  视图页面名称：
        return "item/index";
    }
}
