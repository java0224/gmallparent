package com.atguigu.gmall.all.controller;

import com.atguigu.gmall.model.order.OrderInfo;
import com.atguigu.gmall.order.client.OrderFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author 花鸟の虫鱼
 * @CreateDate 21/8/27 17:17
 * @Version 1.0
 * @Description TODO
 */
@Controller
public class PaymentController {
    @Autowired
    private OrderFeignClient orderFeignClient;

    //
    @GetMapping("pay.html")
    public String pay(Long orderId, Model model) {
        //  后台需要保存一个orderInfo对象
        OrderInfo orderInfo = orderFeignClient.getOrderInfo(orderId);
        model.addAttribute("orderInfo", orderInfo);
        //  返回页面视图名称
        return "payment/pay";
    }
}
