package com.atguigu.gmall.all.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author 花鸟の虫鱼
 * @CreateDate 21/8/20 21:22
 * @Version 1.0
 * @Description TODO
 */
@Controller
public class PassportController {

    //  http://passport.gmall.com/login.html?originUrl=http://www.gmall.com/
    //  从哪里点击的URL 被记录在 originUrl 参数中！
    @GetMapping("login.html")
    public String login(HttpServletRequest request){
        //  登录完成之后，调回到原来的的页面！
        String originUrl = request.getParameter("originUrl");
        //  保存到作用域中
        request.setAttribute("originUrl",originUrl);
        //  返回登录页面！
        return "login";
    }

}

